# How to use it.

## Post install

```shell script
wget -O - https://gitlab.com/amouhzi/kimsufi-k8s-debian-post-install/-/raw/master/post-install.sh | \
  ssh YOUR_ROOT_USER@YOUR_HOST 'sh -s install'
```

## Update node

```shell script
wget -O - https://gitlab.com/amouhzi/kimsufi-k8s-debian-post-install/-/raw/master/post-install.sh | \
  ssh YOUR_ROOT_USER@YOUR_HOST 'sh -s update NODE_NAME'
```
