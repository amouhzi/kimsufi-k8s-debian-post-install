#!/bin/sh

set -e

dist_id=$(lsb_release -is | tr '[:upper:]' '[:lower:]')
codename=$(lsb_release -cs)

#Black        0;30
#Red          0;31
#Green        0;32
#Yellow       0;33
#Blue         0;34
#Magenta      0;35
#Cyan         0;36
#Light Gray   0;37

red='\033[0;31m'
green='\033[0;32m'
yellow='\033[0;33m'
bold="\033[1m"
reset='\033[0m'

if [ "$(whoami)" != "root" ]; then
  SUDO=sudo
fi

red() {
  printf "$red%s$reset" "$1"
}

green() {
  printf "$green%s$reset" "$1"
}

yellow() {
  printf "$yellow%s$reset" "$1"
}

bold() {
  printf "$bold%s$reset" "$1"
}

ok() {
  green "[OK]"
  echo
}

fail() {
  red "[FAIL]"
  echo
  red "$1"
  echo
  exit 1
}

skipped() {
  yellow "[SKIPPED]"
  echo
}

checking() {
  bold "Checking $1"
  printf " ... "
}

check_os() {
  checking "OS"
  if [ "$dist_id" != "debian" ]; then fail "This script is only tested on debian."; fi
  ok

  checking "$dist_id version"
  if [ "$codename" != "buster" ]; then fail "This script is not tested on $codename, it is only tested on debian buster."; fi
  ok
}

check_ram() {
  total=$(awk '/^MemTotal:/{print $2}' /proc/meminfo)

  checking "RAM"
  if [ "$total" -lt 2097152 ]; then fail "Kubernetes needs 2 GB or more of RAM per machine."; fi
  ok
}

check_cpu() {
  total=$(grep -c ^processor /proc/cpuinfo)

  checking "CPU cores"
  if [ "$total" -lt 2 ]; then fail "Kubernetes needs 2 CPUs or more."; fi
  ok
}

disable_swap() {
  printf "Disabling swap ... "

  ${SUDO} /sbin/swapoff -a
  ${SUDO} sed -i '/^[^#].* swap /  s/^/#/g' /etc/fstab

  ok
}

check_swap() {
  total=$(awk '/^SwapTotal:/{print $2}' /proc/meminfo)

  checking "Swap"
  if [ "$total" -gt 0 ]; then fail "You MUST disable swap in order for the kubelet to work properly. Execute disable_swap to do it for you"; fi
  ok
}

fix_br_netfilter() {
  bold "Loading br_netfilter module ... "

  if lsmod | grep -q br_netfilter; then
    skipped
  else
    echo br_netfilter | ${SUDO} tee -a /etc/modules-load.d/modules.conf >/dev/null
    ${SUDO} modprobe br_netfilter

    ok
  fi
}

check_br_netfilter() {
  bold "Making sure that the br_netfilter module is loaded ... "

  if lsmod | grep -q br_netfilter; then
    ok
  else
    fail "br_netfilter module is not loaded."
  fi
}

fix_sysctl_param() {
  param=$1
  bold "Setting $param to 1 ... "

  if ${SUDO} sysctl "$param" | grep -q " = 1"; then
    skipped
  else
    echo "$param = 1" | ${SUDO} tee "/etc/sysctl.d/$param.conf" >/dev/null
    ok
  fi
}

apply_sysctl_params() {
  bold "Applying new sysctl configuration ... "
  echo
  ${SUDO} sysctl --system
  bold "Applying new sysctl configuration ... "
  ok
}

fix_bridged_traffic() {
  fix_sysctl_param net.bridge.bridge-nf-call-iptables
  fix_sysctl_param net.bridge.bridge-nf-call-ip6tables
  fix_sysctl_param net.ipv4.ip_forward
  apply_sysctl_params
}

check_bridged_traffic() {
  bold "Ensuring net.bridge.bridge-nf-call-iptables is set to 1 ... "

  if ${SUDO} sysctl net.bridge.bridge-nf-call-iptables | grep -q " = 1"; then
    ok
  else
    fail "net.bridge.bridge-nf-call-iptables is not set to 1."
  fi
}

fix_overlay() {
  bold "Loading overlay module ... "

  if lsmod | grep -q overlay; then
    skipped
  else
    echo overlay | ${SUDO} tee -a /etc/modules-load.d/modules.conf >/dev/null
    ${SUDO} modprobe overlay

    ok
  fi
}

check_overlay() {
  bold "Making sure that the overlay module is loaded ... "

  if lsmod | grep -q overlay; then
    ok
  else
    fail "overlay module is not loaded."
  fi
}

installing_crio() {
  bold "Installing cri-o ... "
  echo

  ${SUDO} sh -c "echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_10/ /' >/etc/apt/sources.list.d/crio.list"
  wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/Debian_10/Release.key -O- |
    ${SUDO} APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 apt-key add -
  ${SUDO} apt-get update -qq
  ${SUDO} apt-get -y install cri-o-1.17

  bold "cri-o installed ... "
  ok
}

starting_crio() {
  bold "Starting CRI-O ... "

  ${SUDO} systemctl daemon-reload
  ${SUDO} systemctl start crio

  ok
}

install_kube_tools() {
  bold "Installing kube tools ... "
  echo

  ${SUDO} apt-get update && ${SUDO} apt-get install -y apt-transport-https curl
  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | ${SUDO} APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 apt-key add -
  printf "deb https://apt.kubernetes.io/ kubernetes-xenial main" | ${SUDO} tee /etc/apt/sources.list.d/kubernetes.list >/dev/null
  ${SUDO} apt-get update
  ${SUDO} apt-get install -y kubelet kubeadm kubectl
  ${SUDO} apt-mark hold kubelet kubeadm kubectl

  bold "Kube tools installed ... "
  ok
}

configure_swappiness() {
  bold "Configuring swappiness ... "

  echo "vm.swappiness = 5" | ${SUDO} tee "/etc/sysctl.d/vm.swappiness.conf" >/dev/null

  ok

  apply_sysctl_params
}

configure_time() {
  bold "Configuring server time ... "

  printf "Etc/UTC" | ${SUDO} tee /etc/timezone >/dev/null
  ${SUDO} dpkg-reconfigure -f noninteractive tzdata
  ${SUDO} apt-get install -y ntp

  bold "Server time configured ... "
  ok
}

init_kube() {
  bold "Initializing the control-plane node ... "
  echo

  ${SUDO} kubeadm config images pull

  ${SUDO} kubeadm init --pod-network-cidr=10.244.0.0/16
}

check_iptables_one() {
  if update-alternatives --display "$1" | grep "link currently points to /usr/sbin/$1-nft" >/dev/null; then
    fail "This nftables backend is not compatible with the current kubeadm packages. Execute fix_iptables"
  fi
}

check_iptables() {
  checking "iptables"

  check_iptables_one iptables
  check_iptables_one ip6tables
  check_iptables_one arptables
  check_iptables_one ebtables

  ok
}

fix_iptables() {
  printf "Setting up legacy iptables ... "

  ${SUDO} apt install -y arptables ebtables

  ${SUDO} update-alternatives --set iptables /usr/sbin/iptables-legacy
  ${SUDO} update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
  ${SUDO} update-alternatives --set arptables /usr/sbin/arptables-legacy
  ${SUDO} update-alternatives --set ebtables /usr/sbin/ebtables-legacy
}

install_package_less_than_version() {
  package="$1"
  version="$2"

  fullVersion="0"

  for v in $(apt-cache madison "$package" | awk -F'|' '{print $2}'); do
    if dpkg --compare-versions "$v" lt "$version"; then
      if dpkg --compare-versions "$v" gt $fullVersion; then
        fullVersion=$v
      fi
    fi
  done

  if [ "$fullVersion" = "0" ]; then
    echo "There is no version for $package:$version"
    exit 1
  fi

  ${SUDO} apt-get install -y --allow-downgrades --allow-change-held-packages "$package=$fullVersion"
}

install_docker() {
  printf "Installing docker ... "

  ${SUDO} apt-get remove docker docker.io runc
  ${SUDO} apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
  curl -fsSL "https://download.docker.com/linux/$dist_id/gpg" | ${SUDO} APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 apt-key add -
  ${SUDO} add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$dist_id $codename stable"
  ${SUDO} apt-get update
  install_package_less_than_version docker-ce-cli 5:18.10
  install_package_less_than_version docker-ce 5:18.10
  ${SUDO} apt-get install -y containerd.io
  ${SUDO} apt-mark hold docker-ce docker-ce-cli

  {
    printf "{\n"
    printf "  \"exec-opts\": [\"native.cgroupdriver=systemd\"],\n"
    printf "  \"log-driver\": \"json-file\",\n"
    printf "  \"log-opts\": {\"max-size\": \"100m\"},\n"
    printf "  \"storage-driver\": \"overlay2\" \n"
    printf "}\n"
  } | ${SUDO} tee /etc/docker/daemon.json >/dev/null

  ${SUDO} mkdir -p /etc/systemd/system/docker.service.d

  ${SUDO} systemctl daemon-reload
  ${SUDO} systemctl restart docker
}

ask_confirmation() {
  message=$1

  while true; do
    printf "%s [yes/no] " "$message"
    read -r result

    case $result in
    Yes | yes | Y | y)
      return 0
      break
      ;;
    No | no | N | n)
      return 1
      break
      ;;
    *) echo "Please answer yes or no." ;;
    esac
  done
}

reset_kubernetes() {
  if ask_confirmation "Are you sure to reset the Kubernetes?"; then
    ${SUDO} kubeadm reset --force
    init_kube
  else
    echo "NOK"
  fi
}

kube_ctl() {
  ${SUDO} kubectl --kubeconfig='/etc/kubernetes/admin.conf' "$@"
}

check_kube_version() {
  checking "installed Kubernetes version"

  version=$(${SUDO} kubectl --kubeconfig='/etc/kubernetes/admin.conf' version --short | grep Server |
    sed -n -e 's/Server Version: v\([0-9]\{1,2\}\.[0-9]\{1,2\}\.[0-9]\{1,2\}\)$/\1/p')

  major=$(echo "$version" | sed -n -e 's/\([0-9]\{1,2\}\)\.[0-9]\{1,2\}\.[0-9]\{1,2\}$/\1/p')
  minor=$(echo "$version" | sed -n -e 's/[0-9]\{1,2\}\.\([0-9]\{1,2\}\)\.[0-9]\{1,2\}$/\1/p')
  fix=$(echo "$version" | sed -n -e 's/[0-9]\{1,2\}\.[0-9]\{1,2\}\.\([0-9]\{1,2\}\)$/\1/p')

  versionInt=$(printf "%d%02d%02d" "$major" "$minor" "$fix")

  if [ "$versionInt" -lt 11700 ]; then
    fail "Version less than 1.17.0 is not supported."
  fi

  ok
}

update() {
  check_kube_version
  disable_swap
  check_swap

  ${SUDO} apt update
  ${SUDO} apt-get install -y --allow-change-held-packages kubeadm=1.18.3-00
  kube_ctl drain "$1" --ignore-daemonsets --delete-local-data
  ${SUDO} kubeadm upgrade plan
  ${SUDO} kubeadm upgrade apply -y v1.18.3
  ${SUDO} apt-get dist-upgrade -y
  kube_ctl uncordon "$1"
  ${SUDO} apt-get install -y --allow-change-held-packages kubelet=1.18.3-00 kubectl=1.18.3-00
  ${SUDO} systemctl restart kubelet
}

install() {
  check_os
  check_ram
  check_cpu
  disable_swap
  check_swap
  fix_br_netfilter
  check_br_netfilter
  fix_bridged_traffic
  check_bridged_traffic
  fix_overlay
  check_overlay
  installing_crio
  starting_crio
  install_kube_tools
  configure_swappiness
  configure_time
  init_kube
  apt-get autoremove --purge exim4-daemon-light
}

"$@"
